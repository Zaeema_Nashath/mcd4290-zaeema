// Week 3 coding exercise JS file

question1()
question2()
question4()

//QUESTION 1

function question1(){
	let output = ""
		
	function objectToHTML(anObject)
	{
		let string = ""
		for (let prop in anObject){
			string += prop + ": " + anObject[prop] + "\n"
		}
	return string
	}
	
	var testObj = {
		number: 1,
		string: "abc",
		array: [5, 4, 3, 2, 1],
		boolean: true
	};
	
	output = "QUESTION 1: \n\n" + objectToHTML(testObj)
	
	let outPutArea = document.getElementById("outputArea1")
	outPutArea.innerText = output
}

// QUESTION 2

function question2(){
	var outputAreaRef = document.getElementById("outputArea2");
	var output = "";
	
	function flexible(fOperation, operand1, operand2)
	{
		var result = fOperation(operand1, operand2);
		return result;
	}
	
	function addition(num1,num2)
	{
		return num1+num2
	}
	
	let product = function(num1,num2)
	{
		return num1*num2
	}
	
	output += "QUESTION 2: <br/><br/>" + flexible(addition, 3, 5) + "<br/>";
	output += flexible(product, 3, 5) + "<br/>";
	
	outputAreaRef.innerHTML = output;
}

//QUESTION 3:

/*
 * create a function that takes an array as its parameter
 * set a min value and a max value to the first elements of the array
 * use a for loop that loops through all the elements starting from the second element
 * use an if ststement to check if each element is less than the current min, if it is then make that element the min
 * use another if ststement to check if each element is more than the current max, if it is then make that element the max
*/

// QUESTION 4

function question4(){
	let output = ""
	
	function extremeValues(anArray)
	{
		let min = anArray[0]
		let max = anArray[0]
		for (i=1; i<=anArray.length; i++)
		{
			if (anArray[i]<min)
			{
				min = anArray[i]
			}
			
			if (anArray[i]>max)
			{
				max = anArray[i]
			}
		}
		
		let string = "Maximum value: " + max + "\n Minimum Value: " + min
		return string
	}
	
	var values = [4, 3, 6, 12, 1, 3, 8];
	
	output = "QUESTION 4: \n\n " + extremeValues(values)
	let outPutArea = document.getElementById("outputArea3")
	outPutArea.innerText = output
}