 
 function doIt(){
	let num1 = parseInt(document.getElementById("number1").value)
	let num2 = parseInt(document.getElementById("number2").value)
	let num3 = parseInt(document.getElementById("number3").value)
	let sum = num1 + num2 + num3
	
	
	let answerRef = document.getElementById("answer")
	let evenOddRef = document.getElementById("evenOdd")
	answerRef.innerHTML = sum;
	
	
	//checking if answer is positive or negative
	if (sum > 0)
	{
		answerRef.classList.add("positive") 
	}
	
	if (sum < 0)
	{
		answerRef.classList.add("negative")
	}
	
	
	//checking if number is even or odd
	let even_odd=""
	
	if (sum%2 === 0)
	{
		even_odd = "(even)";
		evenOddRef.innerHTML = even_odd;
		evenOddRef.classList.add("even")
	}
	
	if (sum%2 !== 0)
	{
		even_odd = "(odd)";
		evenOddRef.innerHTML = even_odd;
		evenOddRef.classList.add("odd")
	}
	
 }