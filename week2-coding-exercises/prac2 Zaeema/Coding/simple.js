//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = "" //empty output, fill this so that it can print onto the page.
    
    //Question 1 here 
    
	let data = [54,-16,80,55,-74,73,26,5,-34,-73,19,63,-55,-61,65,-14,-19,-51,-17,-25];
	let positiveOdd = [];
	let negativeEven = [];
	for (let i=0; i<data.length;i++)
  {
    if ((data[i] % 2 !== 0) && (data[i] > 0)) //checking if the element is positive odd and adding it to the relevant array
      {
        positiveOdd.push(data[i]);
      }
    else if ((data[i] % 2 === 0) && (data[i] < 0)) //checking if the element is negative even and adding it to the relevant array
      {
        negativeEven.push(data[i]);
      }
  }

	output = "QUESTION 1\n\n" + "Positive Odd: " + positiveOdd + "\nNegative Even:" + negativeEven;
	
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2(){
    let output = "" 
    
    //Question 2 here 
	
	let one=0, two=0, three=0, four=0, five=0, six=0;
	var diceRoll;
	
	for (i=0;i<60000;i++)
	{
		diceRoll = Math.floor((Math.random() * 6) + 1); // generating random die rolls from 1-6 and recording the frequency of each number in different variables
		if (diceRoll === 1)
			{
				one += 1;
			}
		else if (diceRoll === 2)
			{
				two += 1;
			}
		else if (diceRoll === 3)
			{
				three += 1;
			}
		else if (diceRoll === 4)
			{
				four += 1;
			}
		else if (diceRoll === 5)
			{
				five += 1;
			}	
		else if (diceRoll === 6)
			{
				six += 1;
			}
	}
	
    output = "QUESTION 2\n\n" + "Frequency of die rolls\n" + "1: " + one + "\n2: " + two + "\n3: " + three +  "\n4: " + four + "\n5: " + five + "\n6: " + six;
	
	let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    let output = "" 
    
    //Question 3 here 
	
	let one=0, two=0, three=0, four=0, five=0, six=0;
	var rollArray=[0,0,0,0,0,0,0]; // empty array to capture frequencies (0th element won't be used)
	
	for (i=0;i<60000;i++)
		{
			diceRoll = Math.floor((Math.random() * 6) + 1);
			rollArray[diceRoll] += 1
		}
    
	output = "QUESTION 3\n\n" + "Frequency of die rolls\n" + "1: " + rollArray[1] + "\n2: " + rollArray[2] + "\n3: " + rollArray[3] + "\n4: " + rollArray[4] + "\n5: " + rollArray[5] + "\n6: " + rollArray[6];
	
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4(){
    let output = "" 
    
    //Question 4 here

	var dieRolls = {
			Frequencies: {
				1:0,
				2:0,
				3:0,
				4:0,
				5:0,
				6:0,
			},
			Total: 60000,
			Exceptions: ""
	}
	
	for (i=0;i<dieRolls.Total;i++)
	{
		diceRoll = Math.floor((Math.random() * 6) + 1);  // generating random die rolls from 1-6 and recording the frequency of each number in the Frequencies object
		if (diceRoll === 1)
			{
				dieRolls.Frequencies["1"] += 1;
			}
		else if (diceRoll === 2)
			{
				dieRolls.Frequencies["2"] += 1;
			}
		else if (diceRoll === 3)
			{
				dieRolls.Frequencies["3"] += 1;
			}
		else if (diceRoll === 4)
			{
				dieRolls.Frequencies["4"] += 1;
			}
		else if (diceRoll === 5)
			{
				dieRolls.Frequencies["5"] += 1;
			}	
		else if (diceRoll === 6)
			{
				dieRolls.Frequencies["6"] += 1;
			}
	}
	
	for (prop in dieRolls.Frequencies)
	{
		if (dieRolls.Frequencies[prop]> 10100 || dieRolls.Frequencies[prop] < 9900)
		{
			dieRolls.Exceptions += " " + prop
		}
	}
    
	output = "QUESTION 4\n\n" + "Frequency of dice rolls\n" + "Total rolls: " + dieRolls.Total + "\nFrequencies: " + "\n1: " + dieRolls.Frequencies["1"] + "\n2: " + dieRolls.Frequencies["2"] + "\n3: " + dieRolls.Frequencies["3"] + "\n4: " + dieRolls.Frequencies["4"] + "\n5: " + dieRolls.Frequencies["5"] + "\n6: " + dieRolls.Frequencies["6"] + "\nExceptions: " + dieRolls.Exceptions
	
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5(){
    let output = "" 
    
    //Question 5 here

	let person = {
		name: "Jane",
		income: 127050
	}
	
	let tax
	
	if (person.income>=0 && person.income<=18200)
	{
		
	}
	else if (person.income>=18201 && person.income<=37000)
		{
			tax = (person.income - 18200) * 0.19
		}
	else if (person.income>=37001 && person.income<=90000)
		{
			tax = ((person.income - 37000) * 0.325) + 3572
		}
	else if (person.income>=90001 && person.income<=180000)
		{
			tax = ((person.income - 90000) * 0.37) + 20797
		}
	else if (person.income>=180001)
		{
			tax = ((person.income - 180000) * 0.45) + 54097
		}
    
	output = "QUESTION 5\n\n" + person.name + "\'s income is: $" + person.income +", and her tax owed is: $" + tax.toFixed(2)
	
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}